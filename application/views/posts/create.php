
    <div class="coverheader padingtop50 padingbottom50">

<div class="row">
  <div class="col-md-8">
    
      <h2><?= $title; ?></h2>

      <?php echo validation_errors(); ?>

      <?php echo form_open_multipart('posts/create'); ?>
      <div class="form-group">
        <label>Title</label>
 
        <input type="text" class="input" name="title" placeholder="Add Title">
      </div>
      <div class="form-group">
        <label>Body</label>
        <textarea id="post_content" class="form-control" name="body" placeholder="Add Body"></textarea>
      </div>

        <p>Category</p> 
      <div class="select">
        <select name="category_id">
          <?php foreach($categories as $category): ?>
            <option value="<?php echo $category['id']; ?>"><?php echo $category['categories']; ?></option>
          <?php endforeach; ?>
        </select>
      </div>
  

<div class="file has-name">
  <label class="file-label">
    <input class="file-input" type="file" name="userfile">
    <span class="file-cta">
      <span class="file-icon">
        <i class="fas fa-upload"></i>
      </span>
      <span class="file-label">
        Choose a file…
      </span>
    </span>
    <span class="file-name">
      Screen Shot 2017-07-29 at 15.54.25.png
    </span>
  </label>
</div>
     
      <button type="submit" class="button button-primary">Submit</button>
    </form>
  </div>
  <div class="col-md-4">as</div>
</div>





  </div>
</div>
</section>