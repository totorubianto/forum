<div class="coverheader padingtop50 padingbottom50">
	<div class="columns is-3">
		<div class="column is-two-thirds">
			<div class="coverisi padingtop20 padingbottom20">
				<p class="bold textjudul padingbottom10">
					<i class="fa fa-fire white lingkaran"></i> Tread Terbaru
				</p>
				<div class="backgroundwhite coverinnya border">
					<div class="columns is-gapless">
						<div class="column is-one-third">
							<div class="covergambar">
								<img src="./assets/img/card/1.jpeg">
							</div>
						</div>
						<div class="column">
							<div class="isicard pading20">
								<p class="judul">Emago Menambah 20 Game Terbaru di Bulan Juli</p>

								<p class="iconkecil"><i class="fa fa-plug"></i>Tema    |    <i class="fa fa-user"></i>Toto Rubianto    |    <i class="fa fa-clock-o"></i>17 Januari 2001</p>

								<p class="deskripsi">"Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit..."
									"There is no one who loves pain itself
								</p>
								<a class="margintop10" href="#">Selengkapnya</a>
							</div>
						</div>
					</div>
				</div>
				<div class="row ">
					<?php foreach($posts as $post) : ?>
						<div class="col-md-3 padingtop20">
							<div class="backgroundwhite coverinnya border">
								<div class="covergambar1">
									<img src="<?php echo site_url(); ?>assets/images/posts/<?php echo $post['post_image']; ?>">
								</div>
								<div class="cardkecilisi pading10">
									<a href="<?php echo site_url('/posts/'.$post['slug']); ?>" class="judul2"><?php echo $post['title']; ?></a>
									<p class="kategori"><?php echo $post['categories']; ?></p>
								</div>
							</div>
						</div>
					<?php endforeach; ?>
				</div>
			</div>
		</div>
		<div class="column">
			<div class="coverisi padingtop20 padingbottom20">
				<p class="bold textjudul padingbottom10">
					<i class="fa fa-fire white lingkaranhijau"></i> Tread Hot
				</p>
				<div class="backgroundwhite border">
					<div class="pading10">
						<div class="columns is-gapless">
							<div class="column is-one-quarter">
								<img src="./assets/img/card/1.jpeg">
							</div>
							<div class="column">
								<div class="padingleftright10">
									<p class="sidebarjudul">sasa</p>
									<p>asadsdasd</p>
									<p>asadsdasd</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

	</div>
</div>
</div>
</section>




