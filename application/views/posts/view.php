<div class="row">
	<div class="col-md-8">
		<div class="coverheader padingtop50 padingbottom50">
			<div class="backgroundwhiteitem pading20 border marginbottom10">
				<nav class="breadcrumb is-small" aria-label="breadcrumbs">
					<ul>
						<li><a href="<?php echo base_url(); ?>posts">Home</a></li>
						<li><a href="#"><?php echo $post['categories']; ?></a></li>
						<li><a href="#"><?php echo $post['slug']; ?></a></li>
					</ul>

				</nav>
			</div>
			
			<div class="viewpost pading20 backgroundwhiteitem border">
				<div class="row">
					<div class="col-md-1"><div class="pp">a</div></div>
					<div class="col-md-11"><a href="#">
						<a href="<?php echo base_url(); ?>profile"><?php echo $post['name']; ?></a>
						<p class="margintop5 font13" href="#">2 Tread</p>
					</div>
				</div>
				
				
				<hr>
				<p>
					<a class="judul"><?php echo $post['title']; ?></a>
				</p>
				<a class="post-date font12">Posted on: <?php echo $post['created_at']; ?></a><br>

				<img class="margintop10" src="<?php echo site_url(); ?>assets/images/posts/<?php echo $post['post_image']; ?>">
				<div class="post-body">
					<?php echo $post['body']; ?>
				</div>

				<?php if($this->session->userdata('user_id') == $post['user_id']): ?>
					<hr>
					<a class="btn btn-default pull-left" href="<?php echo base_url(); ?>posts/edit/<?php echo $post['slug']; ?>">Edit</a>
					<?php echo form_open('/posts/delete/'.$post['id']); ?>
					<input type="submit" value="Delete" class="btn btn-danger">
				</form>
			<?php endif; ?>
		</div>
		<hr>
		<!-- disqus komentar -->
		<div id="disqus_thread"></div>
		<script>
			(function() { 
				var d = document, s = d.createElement('script');
				s.src = 'https://emago-1.disqus.com/embed.js';
				s.setAttribute('data-timestamp', +new Date());
				(d.head || d.body).appendChild(s);
			})();
		</script>
		<noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
	</div>
	<div class="col-md-4">
	</div>
</div>
</div>
</div>
</section>



