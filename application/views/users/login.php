
<div class="coverheader padingtop50 padingbottom50">


	<div id="root" class="container">
		<div class="row">	
			<div class="col-md-8">
				<img src="<?php echo base_url(); ?>assets/img/totosa.png" 	>
			</div>
			<div class="col-md-4">
				<div class="backgroundwhite pading30 border">
					<tabs>
						<tab name="SignIn" :selected="true">
							<?php echo form_open('users/login'); ?>
							<div class="field">
								<div class="columns is-1">
									<div class="column">
										<a class="button is-gmail width100">
											<span class="icon">
												<i class="fa fa-google"></i>
											</span>
											<span>Google</span>
										</a>
									</div>
									<div class="column">
										<a class="button is-facebook width100">
											<span class="icon">
												<i class="fa fa-facebook"></i>
											</span>
											<span>Facebook</span>
										</a>
									</div>
								</div>

								
								<p class="margintopbottom10">Username</p>
								<p class="control has-icons-left has-icons-right">
									<input class="input" name="username" type="text" placeholder="Username" required autofocus>
									<span class="icon is-small is-left">
										<i class="fa fa-envelope"></i>
									</span>
								</p>

							</div>
							<div class="field">
								<p class="margintopbottom10">Password</p>
								<p class="control has-icons-left">
									<input class="input" name="password" type="password" placeholder="Password" required autofocus>
									<span class="icon is-small is-left">
										<i class="fa fa-user"></i>
									</span>
								</p>
							</div>
							<label class="checkbox">
								<input type="checkbox">
								Remember me
							</label>
							<hr>
							<button type="submit" class="button is-primary width100">Login</button>
							<?php echo form_close(); ?>

						</tab>
						<tab name="SignUp">



							<?php echo validation_errors(); ?>

							<?php echo form_open('users/register'); ?>

							<div class="field">
								<p class="margintopbottom10">Name</p>
								<input type="text" class="input" name="name" placeholder="Name">
							</div>
							<div class="field">
								<p class="margintopbottom10">Zipcode</p>
								<input type="text" class="input" name="zipcode" placeholder="Zipcode">
							</div>
							<div class="field">
								<p class="margintopbottom10">Email</p>
								<input type="email" class="input" name="email" placeholder="Email">
							</div>
							<div class="field">
								<p class="margintopbottom10">Username</p>
								<input type="text" class="input" name="username" placeholder="Username">
							</div>
							<div class="field">
								<p class="margintopbottom10">Password</p>
								<input type="password" class="input" name="password" placeholder="Password">
							</div>
							<div class="field">
								<p class="margintopbottom10">Confirm Password</p>
								<input type="password" class="input" name="password2" placeholder="Confirm Password">
							</div>
							<button type="submit" class="button is-primary">Submit</button>
							
							<?php echo form_close(); ?>
						</tab>
					</tabs>
				</div>
			</div>
		</div>
	</div>


</div>
</div>
</section>

