		</div>
		<section class="hero itemsemu">
			<div class="hero-body">
				<div class="container">
					<h1 class="title">
						Primary title
					</h1>
					<h2 class="subtitle">
						Primary subtitle
					</h2>
				</div>
			</div>
		</section>
		<div class="width100 itemmedium pading10 white center font12">Copyright © 2018. by EMAGO | The First Cloud Gaming in Indonesia.</div>


		<script type="text/javascript" src="<?php echo base_url(); ?>/assets/tinymce/tinymce.min.js"></script>
		<script type="text/javascript">
			tinymce.init({
				selector: "#post_content",
				plugins: [
				"advlist autolink lists link image charmap print preview hr anchor pagebreak",
				"searchreplace wordcount visualblocks visualchars code fullscreen",
				"insertdatetime nonbreaking save table contextmenu directionality",
				"emoticons template paste textcolor colorpicker textpattern"
				],
				toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image responsivefilemanager",
				automatic_uploads: true,
				image_advtab: true,
				images_upload_url: '<?php echo base_url(); ?>posts/tinymce_upload',
				file_picker_types: 'image', 
				paste_data_images:true,
				relative_urls: false,
				remove_script_host: false,
				file_picker_callback: function(cb, value, meta) {
					var input = document.createElement('input');
					input.setAttribute('type', 'file');
					input.setAttribute('accept', 'image/*');
					input.onchange = function() {
						var file = this.files[0];
						var reader = new FileReader();
						reader.readAsDataURL(file);
						reader.onload = function () {
							var id = 'post-image-' + (new Date()).getTime();
							var blobCache =  tinymce.activeEditor.editorUpload.blobCache;
							var blobInfo = blobCache.create(id, file, reader.result);
							blobCache.add(blobInfo);
							cb(blobInfo.blobUri(), { title: file.name });
						};
					};
					input.click();
				}
			});
		</script>



		<script id="dsq-count-scr" src="//EXAMPLE.disqus.com/count.js" async></script>
		<script src='https://cdnjs.cloudflare.com/ajax/libs/vue/2.1.10/vue.min.js'></script>
		<script  src="<?php echo base_url(); ?>assets/js/vue_tab.js"></script>
	</body>
	</html>