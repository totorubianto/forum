
      <!DOCTYPE html>
      <html>
      <head>
        <title>Emago</title>
        <meta charset="UTF-8">
        <meta name="description" content="Free Web tutorials">
        <meta name="keywords" content="HTML,CSS,XML,JavaScript">
        <meta name="author" content="John Doe">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bulma.css"/>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap-grid.css"/>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/style.css"/>
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
      </head>
      <body>
        <nav class="navtop"></nav>
        <div class="container">
          <nav class="navbar ">
            <div class="navbar-brand">
              <a class="navbar-item" href="http://bulma.io">
                <img src="<?php echo base_url(); ?>assets/img/logo2.png" width="112" height="28">
              </a>
              <div class="navbar-burger burger" data-target="navMenubd-example">
                <span></span>
                <span></span>
                <span></span>
              </div>
            </div>
            <div id="navMenubd-example" class="navbar-menu">
              <div class="navbar-start">
                <a class="navbar-item  is-active" href="<?php echo base_url(); ?>posts">
                  Home
                </a>
                <div class="navbar-item has-dropdown is-hoverable is-mega">
                  <div class="navbar-link">
                    News
                  </div>
                  <div id="blogDropdown" class="navbar-dropdown " data-style="width: 18rem;">
                    <div class="container is-fluid">
                      <div class="row">
                        <div class="col-md-3">as</div>
                        <div class="col-md-3">as</div>
                        <div class="col-md-3">as</div>
                        <div class="col-md-3">as</div>
                      </div>
                    </div>
                    <hr class="navbar-divider">
                    <div class="navbar-item">
                      <div class="navbar-content">
                        <div class="level is-mobile">
                          <div class="level-left">
                            <div class="level-item">
                              <strong>Stay up to date!</strong>
                            </div>
                          </div>
                          <div class="level-right">
                            <div class="level-item">
                              <a class="button bd-is-rss is-small" href="<?php echo base_url(); ?>categories">
                                <span class="icon is-small">
                                  <i class="fa fa-rss"></i>
                                </span>
                                <span>Subscribe</span>
                              </a>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="navbar-item has-dropdown is-hoverable">
                  <div class="navbar-link">
                    More
                  </div>
                  <div id="moreDropdown" class="navbar-dropdown ">
                    <a class="navbar-item " href="http://bulma.io/extensions/">
                      <div class="level is-mobile">
                        <div class="level-left">
                          <div class="level-item">
                            <p>
                              <strong>Extensions</strong>
                              <br>
                              <small>Side projects to enhance Bulma</small>
                            </p>
                          </div>
                        </div>
                        <div class="level-right">
                          <div class="level-item">
                            <span class="icon has-text-info">
                              <i class="fa fa-plug"></i>
                            </span>
                          </div>
                        </div>
                      </div>
                    </a>
                  </div> 
                </div>
              </div>
              <div class="navbar-end">
                <div class="navbar-item">
                  <div class="field is-grouped">
                    <p class="control">
                      <p class="control has-icons-left">
                        <input class="input" type="text" placeholder="Search...">
                        <span class="icon is-small is-left">
                          <i class="fa fa-search"></i>
                        </span>
                      </p>
                    </p>
                    <?php if(!$this->session->userdata('logged_in')) : ?>
                      <p class="control">
                        <a class="button is-primary" href="<?php echo base_url(); ?>users/login">
                          <span class="icon">
                            <i class="fa fa-user"></i>
                          </span>
                          <span>Sign In</span>
                        </a>
                      </p>
                    <?php endif; ?>
                  </div>
                </div>
                <?php if($this->session->userdata('logged_in')) : ?>
                  <div class="navbar-item has-dropdown is-hoverable">
                    <a class="navbar-link  is-active" href="/documentation/overview/start/">
                      <?php echo $this->session->userdata('username'); ?>
                    </a>
                    <div class="navbar-dropdown is-right">
                      <a class="navbar-item " href="<?php echo base_url(); ?>posts/create">
                        Add Tread
                      </a>
                      <a class="navbar-item" href="#">
                        View Tread
                      </a>
                      <hr class="navbar-divider">
                      <a class="navbar-item" href="<?php echo base_url(); ?>users/logout">
                        <strong class="has-text-info">Logout</strong>
                      </a>
                    </div>
                  </div>
                <?php endif; ?>
              </div>
            </div>
          </nav>
        </div>
      </div>


      <section class="backgroundgrey">
        <div class="container">

         <?php if($this->session->flashdata('user_registered')): ?>
          <?php echo '<p class="alert alert-success">'.$this->session->flashdata('user_registered').'</p>'; ?>
        <?php endif; ?>

        <?php if($this->session->flashdata('post_created')): ?>
          <?php echo '<p class="alert alert-success">'.$this->session->flashdata('post_created').'</p>'; ?>
        <?php endif; ?>

        <?php if($this->session->flashdata('post_updated')): ?>
          <?php echo '<p class="alert alert-success">'.$this->session->flashdata('post_updated').'</p>'; ?>
        <?php endif; ?>

        <?php if($this->session->flashdata('category_created')): ?>
          <?php echo '<p class="alert alert-success">'.$this->session->flashdata('category_created').'</p>'; ?>
        <?php endif; ?>

        <?php if($this->session->flashdata('post_deleted')): ?>
          <?php echo '<p class="alert alert-success">'.$this->session->flashdata('post_deleted').'</p>'; ?>
        <?php endif; ?>

        <?php if($this->session->flashdata('login_failed')): ?>
          <?php echo '<p class="alert alert-danger">'.$this->session->flashdata('login_failed').'</p>'; ?>
        <?php endif; ?>

        <?php if($this->session->flashdata('user_loggedin')): ?>
          <?php echo '<p class="alert alert-success">'.$this->session->flashdata('user_loggedin').'</p>'; ?>
        <?php endif; ?>

        <?php if($this->session->flashdata('user_loggedout')): ?>
          <?php echo '<p class="alert alert-success">'.$this->session->flashdata('user_loggedout').'</p>'; ?>
        <?php endif; ?>

        <?php if($this->session->flashdata('category_deleted')): ?>
          <?php echo '<p class="alert alert-success">'.$this->session->flashdata('category_deleted').'</p>'; ?>
        <?php endif; ?>
