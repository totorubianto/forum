<?php
class Posts extends CI_Controller{

	public function index($offset = 0){	

		if(!file_exists(APPPATH.'views/posts/index.php')){
			show_404();
		}	
		$config['base_url'] = base_url() . 'posts/index/';
		$config['total_rows'] = $this->db->count_all('posts');
		$config['per_page'] = 6;
		$config['uri_segment'] = 3;
		$config['attributes'] = array('class' => 'pagination-link');
		$this->pagination->initialize($config);
		$data['title'] = 'Latest Posts';
		$data['posts'] = $this->post_model->get_posts(FALSE, $config['per_page'], $offset);
		$this->load->view('templates/header');
		$this->load->view('posts/index', $data);
		$this->load->view('templates/footer');
	}
	 function tinymce_upload() {
        $config['upload_path'] = '../emago/assets/images';
        $config['allowed_types'] = 'jpg|png|jpeg';
        $config['max_size'] = 0;
        $this->load->library('upload', $config);
        if ( ! $this->upload->do_upload('file')) {
            $this->output->set_header('HTTP/1.0 500 Server Error');
            exit;
        } else {
            $file = $this->upload->data();
            $this->output
                ->set_content_type('application/json', 'utf-8')
                ->set_output(json_encode(['location' => base_url().'../emago/assets/images/'.$file['file_name']]))
                ->_display();
            exit;
        }
    }



	public function view($slug = NULL){
		$data['post'] = $this->post_model->get_posts($slug);
		$post_id = $data['post']['id'];
		if(empty($data['post'])){ 
			show_404();
		}
		$data['title'] = $data['post']['title'];
		$this->load->view('templates/header');
		$this->load->view('posts/view', $data);
		$this->load->view('templates/footer');
	}

	public function create(){
// buat cek login
		if(!$this->session->userdata('logged_in')){
			redirect('users/login');
		}

		$data['title'] = 'Create Post';

		$data['categories'] = $this->post_model->get_categories();

		$this->form_validation->set_rules('title', 'Title', 'required');
		$this->form_validation->set_rules('body', 'Body', 'required');

		if($this->form_validation->run() === FALSE){
			$this->load->view('templates/header');
			$this->load->view('posts/create', $data);
			$this->load->view('templates/footer');
		} else {
// upload image
			$config['upload_path'] = './assets/images/posts';
			$config['allowed_types'] = 'gif|jpg|png';
			$config['max_size'] = '2048';
			$config['max_width'] = '2000';
			$config['max_height'] = '2000';

			$this->load->library('upload', $config);

			if(!$this->upload->do_upload()){
				$errors = array('error' => $this->upload->display_errors());

				$post_image = 'noimage.jpg';
			} else {
				$data = array('upload_data' => $this->upload->data());
				$post_image = $_FILES['userfile']['name'];
			}

			$this->post_model->create_post($post_image);

// flash alert
			$this->session->set_flashdata('post_created', 'Your post has been created');

			redirect('posts');
		}
	}

	public function delete($id){
// Cek login
		if(!$this->session->userdata('logged_in')){
			redirect('users/login');
		}

		$this->post_model->delete_post($id);

// flash alert
		$this->session->set_flashdata('post_deleted', 'Your post has been deleted');

		redirect('posts');
	}

	public function edit($slug){
// Cek login
		if(!$this->session->userdata('logged_in')){
			redirect('users/login');
		}

		$data['post'] = $this->post_model->get_posts($slug);

// Cek user
		if($this->session->userdata('user_id') != $this->post_model->get_posts($slug)['user_id']){
			redirect('posts');

		}

		$data['categories'] = $this->post_model->get_categories();

		if(empty($data['post'])){
			show_404();
		}

		$data['title'] = 'Edit Post';

		$this->load->view('templates/header');
		$this->load->view('posts/edit', $data);
		$this->load->view('templates/footer');
	}

	public function update(){
// cek login
		if(!$this->session->userdata('logged_in')){
			redirect('users/login');
		}

		$this->post_model->update_post();

// flash alert
		$this->session->set_flashdata('post_updated', 'Your post has been updated');

		redirect('posts');
	}
}